module.exports = {
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@sien-technology/eslint-config-ohbot/lib/vue3-base',
  ],
  rules: {
  },
}
