module.exports = {
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@sien-technology/eslint-config-ohbot/lib/base',
  ],
  rules: {
  },
}
