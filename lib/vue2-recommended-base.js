module.exports = {
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@sien-technology/eslint-config-ohbot/lib/base',
  ],
  rules: {
  },
}
