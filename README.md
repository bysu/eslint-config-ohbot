# my custom eslint rule base on vue2/vue3

## Usage


### Installation

require "eslint-plugin-vue@^8.2.0",please update vue2 project for correct version

```sh
yarn add -D @sien-technology/eslint-config-ohbot eslint-plugin-vue
```

### Configuration

Example `.eslintrc.js`:

```js
module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    '@sien-technology/eslint-config-ohbot/lib/vue2-essential-base'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  globals: {
  },
  rules: {

  }
}
```

available extends:
 - `@sien-technology/eslint-config-ohbot/lib/eslint-base`: define custom eslint rules
 - `@sien-technology/eslint-config-ohbot/lib/vue-base`: define custom vue rules
 - `@sien-technology/eslint-config-ohbot/lib/base`: define custom vue & eslint rules
 - `@sien-technology/eslint-config-ohbot/lib/vue2-essential-base`: base on `@sien-technology/eslint-config-ohbot/lib/base` and vue-essential rules
 - `@sien-technology/eslint-config-ohbot/lib/vue2-recommended-base`: base on `@sien-technology/eslint-config-ohbot/lib/base` and vue-recommended rules
 - `@sien-technology/eslint-config-ohbot/lib/vue3-base`: base on `@sien-technology/eslint-config-ohbot/lib/base` and define custom vue3 rules
 - `@sien-technology/eslint-config-ohbot/lib/vue3-essential-base`: base on `@sien-technology/eslint-config-ohbot/lib/vue3-base` and vue3-essential rules
 - `@sien-technology/eslint-config-ohbot/lib/vue3-recommended-base`: base on `@sien-technology/eslint-config-ohbot/lib/vue3-base` and vue3-recommended rules

